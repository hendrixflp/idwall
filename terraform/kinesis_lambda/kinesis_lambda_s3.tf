/*
Author : Hendrigo Cunha

Descrição:

Terraform para provisionamento de Kinesis com firehose para S3 Glacier e Lambda para recuperação de eventos do Kinesis.

*/

# Kinesis Data Stream - kinesis_lambda-id-wall
# Referência : https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kinesis_stream

resource "aws_kinesis_stream" "id_wall_stream" {
  name             = "kinesis-id-wall"
  shard_count      = 1
  retention_period = 48

  shard_level_metrics = [
    "IncomingBytes",
    "OutgoingBytes",
  ]

  tags = {
    Environment = "test"
  }
}

# IAM Role for Lambda
# Referência : https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_function

resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

# Lambda for consume Kinesis and index on Elastic
# Referência : https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_function

data "archive_file" "lambda" {
  type = "zip"
  output_path = "lambda.zip"
  source_dir = "source_code_lambda"
}
resource "aws_lambda_function" "id_wall_lambda" {
  filename      = "${data.archive_file.lambda.output_path}"
  function_name = "index_data"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "service.handler"
  runtime       = "python3.8"

  # The filebase64sha256() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
  # source_code_hash = "${base64sha256(file("lambda_function.zip"))}"
  # source_code_hash = filebase64sha256("lambda_function.zip")



  environment {
    variables = {
      ELASTIC_URL = "https://search-id-wall-evhbgalrt74wwqq6o56vtzec4m.us-east-2.es.amazonaws.com"
    }
  }
}

# Mapping Trigger to Lambda getRecords on Kinesis
resource "aws_lambda_event_source_mapping" "id_wall_kinesis_lambda_mapping" {
    batch_size = 100
    event_source_arn = "${aws_kinesis_stream.id_wall_stream.arn}"
    enabled = true
    function_name = "${aws_lambda_function.id_wall_lambda.arn}"
    starting_position = "LATEST"
}

# S3 to storage eventos from Kinesis
# Referência :  https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket

resource "aws_s3_bucket" "s3_id_wall" {
  bucket = "my-test-id-wall"
  acl    = "private"
}

# IAM Role for Firehose Kinesis to S3
resource "aws_iam_role" "firehose_role" {
  name = "firehose_test_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "firehose.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

# Firehose to delivery Kinesis data to S3
# Referência :   https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kinesis_firehose_delivery_stream

resource "aws_kinesis_firehose_delivery_stream" "st-fh-kn-to-s3-delivery" {
  name        = "kinesis-firehose-to-s3-delivery"
  destination = "extended_s3"

  kinesis_source_configuration {
    kinesis_stream_arn = "${aws_kinesis_stream.id_wall_stream.arn}"
    role_arn           = "${aws_iam_role.firehose_role.arn}"
  }
  #s3_configuration {
  #  role_arn   = aws_iam_role.firehose_role.arn
  #  bucket_arn = aws_s3_bucket.st_s3_sto.arn
  #}

  extended_s3_configuration {
    role_arn        = "${aws_iam_role.firehose_role.arn}"
    bucket_arn      = "${aws_s3_bucket.s3_id_wall.arn}"
    buffer_size     = 60
    buffer_interval = "100"
  }
}

