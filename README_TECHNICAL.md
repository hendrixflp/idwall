### [Projeto de tráfego de consultas e categorização de despesas em larga escala utilizando AWS Cloud e terraform para provisionamento de recursos.]()
##### Referências : https://registry.terraform.io/ https://docs.aws.amazon.com/pt_br/


![Terraform](https://miro.medium.com/max/576/1*aD1NOsKIdJ7cqetwcMYhkA.png)
---

Primeiro passo do nosso tutorial realizar instalação do Terraform, segue link contendo versões e downloads por Sistema Operacional : 

https://www.terraform.io/downloadsæhtml

macOS 64-bit FreeBSD;

Uma vez instalado, caso esteja utilizando Unix ou Mac poderá usar o comando abaixo:

```
$ whereis terraform
$ /usr/local/bin
```

Para validar a instalação com sucesso pode executar no terminal o seguinte comando :

```
$ terraform -version
```


Em seguida tendo as credenciais da AWS criadas, bastar exportar como descrito abaixo :

Para utilizar é bem simples, basta exportar os valores para as variáveis de ambiente 
direto no terminal ou no seu bash profile como descrito abaixo:

```
export AWS_ACCESS_KEY_ID=sua_access_key_id
export AWS_SECRET_ACCESS_KEY=sua_secret_key
export AWS_REGION=sua_regiao
```

![AWSCli](https://miro.medium.com/max/384/1*XJgqVnQeLbpX9aqXGoygUg.png)
---
Será necessário instalar o client de linha de comando da AWS. Aqui eu utilizo o Ubuntu 16.04 e já possuía a versão 3 do python, foi instalado via pip, dessa forma:
```
$ pip install awscli — upgrade
```

Possuo a versão:
```
$ aws — version
aws-cli/1.16.10 Python/3.6.3 Linux/4.15.0–33-generic botocore/1.12.0
```

Talvez seja necessário, ver a documentação para instalar o aws cli, segue o link: 
https://docs.aws.amazon.com/pt_br/cli/latest/userguide/awscli-install-linux.html#awscli-install-linux-path

Eu recomendo instalar o pyenv e o python 3, realizar a instalação via pip que no futuro será mais 
simples a manutenibilidade da aplicação, é possível também realizar a instalação manual, 
de forma similar a do terraform que abordamos acima, baixar o binário, dispor em algum 
diretório e definir o PATH.
Não vou abordar a instalação do pyenv e do pip, não será difícil encontrar no Google, 
uma opção são os links:
http://blog.abraseucodigo.com.br/instalando-qualquer-versao-do-python-no-linux-macosx-utilizando-pyenv.html
https://sempreupdate.com.br/pip-para-gerenciar-pacotes-do-python-no-gnulinux/


![Authy](https://miro.medium.com/max/576/1*xEZlweO7ZxmkaHne4VWsgA.png)

Habilitar o MFA traz uma nova camada de segurança que faz o papel de contra senha. Esse passo não é obrigatório, 
mas torna a conta segura e evita que alguém não autorizado obtenha seu acesso e deixe uma fatura para você pagar.
Vamos baixar o app no seu smartphone, Eu costumo utilizar para gerar o token, um app chamado Authy, pode ser 
obtido diretamente na Play Store ou na Apple Store, para dispositivos Android/IOS. Eu utilizo Android. 
Existem versões para macOS, Windows e também via chrome, mas nunca utilizei, link abaixo para quem deseja 
maiores informações: https://authy.com/.

Finalmente
---

Iremos utilizar o modelo estático para a definição da chave, proposto pela própria Harshcop , existem outras maneiras, 
maiores informações consultem a documentação: https://www.terraform.io/docs/providers/aws/
Vou descrever abaixo um exemplo de criação de recurso com terraform para termos como exemplos/referência de entendimento:

    
    connection.tf
    provider “aws” {
      region = “us-west-2”
      access_key = “sua_access_key”
      secret_key = “sua_secret_key”
    }
    
Inseridos os valores, vale conferir se esta certo. Eu tive problemas nessa parte, no primeiro momento considerei o ultimo caractere: “%”, na chave secret_key, após a remoção deste, entendo que ele não compõe a chave.

```
$ cat connection.tf
```

Feito isso vamos incluir outro componente que será necessário para provisionarmos algo na AWS e assim validar o nosso entendimento, vamos incluir a VPC, não inventamos nada somente seguindo as instruções da Hashicorp através do link: https://www.terraform.io/docs/providers/aws/r/vpc.html, coloque o conteúdo abaixo em um arquivo, no mesmo diretório que esta o seu projeto, eu utilizei o nome de vpc.tf.
Uma boa pratica é utilizar tags, para realizar a identificação de maneira fácil no painel, hoje temos somente um componente, mas no futuro poderão haver outros e cada item deve ter a identificação com seu propósito.

    vpc.tf
    resource “aws_vpc” “poc-terraform-aws” {
      cidr_block = “10.0.0.0/16”
      instance_tenancy = “dedicated”
        tags {
        Name = “poc-terraform-aws”
       }
    }
    
Agora é a hora da verdade, vamos inicializar o terraform, como o comando:

```
$ terraform init
```

    O retorno será algo do tipo:
    terraform init
    Initializing provider plugins…
    - Checking for available provider plugins on https://releases.hashicorp.com...
    - Downloading plugin for provider “aws” (1.35.0)…
    The following providers do not have any version constraints in configuration,
    so the latest version was installed.
    To prevent automatic upgrades to new major versions that may contain breaking
    changes, it is recommended to add version = “…” constraints to the
    corresponding provider blocks in configuration, with the constraint strings
    suggested below.
    * provider.aws: version = “~> 1.35”
    Terraform has been successfully initialized!
    You may now begin working with Terraform. Try running “terraform plan” to see
    any changes that are required for your infrastructure. All Terraform commands
    should now work.
    If you ever set or change modules or backend configuration for Terraform,
    rerun this command to reinitialize your working directory. If you forget, other
    commands will detect it and remind you to do so if necessary.
    Obtemos a mensagem: Terraform has been successfully initialized!, validou o nosso código e inicializou com sucesso. Também foi criado o diretório .terrraform.
    Próximo passo é executar o terraform plan, será exibido o resumo do que será realizado.

```
$ terraform plan
```
No terminal é exibido em cor diferente o que será criado, a partir do plano de execução definido, o interessante é que se abstrai muita coisa, tornando as coisas mais simples e dessa forma evita que sempre reinventamos a roda.
O único componente que iremos incluir é a VPC, com mascara: 10.0.0.0/16 , que nos possibilita a criação de 65.534 hosts. Em um POST futuro irei abordar com mais detalhes esse item.
terraform plan

    Refreshing Terraform state in-memory prior to plan…
    The refreshed state will be used to calculate this plan, but will not be
    persisted to local or remote state storage.
     — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — 
    An execution plan has been generated and is shown below.
    Resource actions are indicated with the following symbols:
    + create
    Terraform will perform the following actions:
    + aws_vpc.poc-terraform-aws
    id: <computed>
    arn: <computed>
    assign_generated_ipv6_cidr_block: “false”
    cidr_block: “10.0.0.0/16”
    default_network_acl_id: <computed>
    default_route_table_id: <computed>
    default_security_group_id: <computed>
    dhcp_options_id: <computed>
    enable_classiclink: <computed>
    enable_classiclink_dns_support: <computed>
    enable_dns_hostnames: <computed>
    enable_dns_support: “true”
    instance_tenancy: “dedicated”
    ipv6_association_id: <computed>
    ipv6_cidr_block: <computed>
    main_route_table_id: <computed>
    tags.%: “1”
    tags.Name: “ṕoc-terraform-aws”
    Plan: 1 to add, 0 to change, 0 to destroy.
     — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — 
    Note: You didn’t specify an “-out” parameter to save this plan, so Terraform
    can’t guarantee that exactly these actions will be performed if
    “terraform apply” is subsequently run.
    
Agora é aplicar, utiliza-se o comando terraform apply, e no final somos questionados a concordar com a proposta apresentada, é só confirmar com yes.
```
$ terraform apply
```

Segue resultado:

    terraform apply
    An execution plan has been generated and is shown below.
    Resource actions are indicated with the following symbols:
    + create
    Terraform will perform the following actions:
    + aws_vpc.poc-terraform-aws
    id: <computed>
    arn: <computed>
    assign_generated_ipv6_cidr_block: “false”
    cidr_block: “10.0.0.0/16”
    default_network_acl_id: <computed>
    default_route_table_id: <computed>
    default_security_group_id: <computed>
    dhcp_options_id: <computed>
    enable_classiclink: <computed>
    enable_classiclink_dns_support: <computed>
    enable_dns_hostnames: <computed>
    enable_dns_support: “true”
    instance_tenancy: “dedicated”
    ipv6_association_id: <computed>
    ipv6_cidr_block: <computed>
    main_route_table_id: <computed>
    tags.%: “1”
    tags.Name: “ṕoc-terraform-aws”
    Plan: 1 to add, 0 to change, 0 to destroy.
    Do you want to perform these actions?
    Terraform will perform the actions described above.
    Only ‘yes’ will be accepted to approve.
    Enter a value: yes
    aws_vpc.poc-terraform-aws: Creating…
    arn: “” => “<computed>”
    assign_generated_ipv6_cidr_block: “” => “false”
    cidr_block: “” => “10.0.0.0/16”
    default_network_acl_id: “” => “<computed>”
    default_route_table_id: “” => “<computed>”
    default_security_group_id: “” => “<computed>”
    dhcp_options_id: “” => “<computed>”
    enable_classiclink: “” => “<computed>”
    enable_classiclink_dns_support: “” => “<computed>”
    enable_dns_hostnames: “” => “<computed>”
    enable_dns_support: “” => “true”
    instance_tenancy: “” => “dedicated”
    ipv6_association_id: “” => “<computed>”
    ipv6_cidr_block: “” => “<computed>”
    main_route_table_id: “” => “<computed>”
    tags.%: “” => “1”
    tags.Name: “” => “ṕoc-terraform-aws”
    aws_vpc.poc-terraform-aws: Still creating… (10s elapsed)
    aws_vpc.poc-terraform-aws: Still creating… (20s elapsed)
    aws_vpc.poc-terraform-aws: Creation complete after 28s (ID: vpc-0661e5fb099dd2f8e)
    Apply complete! Resources: 1 added, 0 changed, 0 destroyed.


### Inventário

###### Kinesis

---

Data Stream : https://us-east-2.console.aws.amazon.com/kinesis/home?region=us-east-2#/streams/details/kinesis-stone/details
    
FireHose to S3 : https://us-east-2.console.aws.amazon.com/firehose/home?region=us-east-2#/details/kinesis-firehose-to-s3-delivery

---

###### S3

---

S3 Glacier  : https://s3.console.aws.amazon.com/s3/buckets/my-test-sto/?region=us-east-2&tab=overview

---
    
###### Lambda

---

Lambda Overview : https://us-east-2.console.aws.amazon.com/lambda/home?region=us-east-2#/functions/index_data?tab=configuration

---

###### Elastic Search

___

Elastic Overview : https://us-east-2.console.aws.amazon.com/es/home?region=us-east-2#domain:resource=stone;action=dashboard;tab=TAB_OVERVIEW_ID
    
Kibana / Relatórios : https://search-stone-evhbgalrt74wwqq6o56vtzec4m.us-east-2.es.amazonaws.com/_plugin/kibana/
    
___
        

### Provisionamento
###### Referência : https://calculator.s3.amazonaws.com/index.html


##### Kinesis com 1 Shard : 
___
Considerando 
     
     Até 1000 registros/sec 
     
     Tamanho máximo do payload de 1kb 
     
     Capacidade estimada de até 2.635 milhões de payloads / mês
     
     Custo : US$ 50,00/mês

___

##### S3 : 
___

Considerando trafego estimado acima

    Custo : US$ 61,00/mês
    
___

##### Lambda : 
___ 

    Custo : US$ 0.27/mês
    
___

##### Elastic Search : 
___ 

    Custo : US$ 146.00/mês
    
___ 


### CUSTO TOTAL MÊS : US$ 257,00/mês

___ 


### Estudo de Caso / Case

    Atualmente temos uma plataforma provisionada com os seguintes configurações : 
    
    Elastic : 2 zonas, 8 Nós, c5.2xlarge.elasticsearch, 1TB ESB, 3 Instâncias master
    
    Kinesis : 12 shards
    
    S3 : On-demand
    
    Lambda : até 40 execuções concorrentes
    
    Com essa configuração estamos dando suporte a 10 bancos parceiros, que trafegam juntos em torno de 60 milhões
    de consultas todos os dias, chegando a atender 1 Bilhão e 800 Milhões de eventos com essa configuração
    com alta demanda e performance.
    