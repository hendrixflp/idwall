[Plataforma de controle e categorização de despesas]()
---
<br/>

#### DESCRIÇÃO 

<br/>

Utilizamos comunicação via API's Rest visando facilitar nossos clientes, mantendo um padrão bem definido e com qualidade.

Além do acesso e distribuição controlada através do uso de API's bem definidas como veremos abaixo.

<br/>

Nossas aplicações/produtos utilizam sdk’s da AWS que permitem o uso do Kinesis[Kinesis](https://docs.aws.amazon.com/pt_br/streams/latest/dev/introduction.html) e [SDK](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/kinesis.html#Kinesis.Client.put_record) viabilizando consultas de forma centralizada e 
padronizada, sempre respeitando os padrões de informações no formato padrão, abaixo desenho da proposta de arquitetura 
que será utilizada:

<br/>

#CONSULTA USUARIO <br/><br/>

Abaixo exemplo de envio: 

POST /integrations/expenses/user

```
{
   "usuario":{
      "identificador":"ID_USUARIO"
   },
   "banco":{
      "usuario_key": "USER",
      "usuario_secret": "SECRET",
      "tipo_resposta": "endpoint",
      "alvo_resposta": "http://xxxx.com"
   },
   "periodo":{
      "data_inicial": 00/00/0000,
      "data_final": 00/00/0000
   }
}
```

<br/>

Abaixo exemplo de resposta:

<br/>

```
{
  "usuario":{
     "identificador":"00000000"
     "requisicão_id": "xpto-00000"
  },
  "despesas": [
     {
              "categoria": {
                 "identificador": "xxx",
                 "nome": "comida"
              }, 
              "compra":{
                 "identificador": "id_compra",
                 "valor": 1200,
                 "data_compra": "xx/xx/xxxx hh:mm:ss"
              }, 
     },
     {
              "categoria": {
                 "identificador": "xxx",
                 "nome": "vestuario",
              }, 
              "compra":{
                 "identificador": "id_compra",
                 "valor": 150,
                 "data_compra": "xx/xx/xxxx hh:mm:ss"
              }
     },
     "observacoes":"Usuário com alto gasto próximo dos seus limites"
  ]
}
```

<br/>

### Arquitetura da Solução

![Arq](arquitetura_idwall.png)

<br/>

Uma vez realizada a integração via API dado um padrão bem definido seguimos, conforme detalhamento da arquitetura acima o envio e processamento de tráfego através da solução de alta volumetria que
por sua vez o realiza o encaminhamento do processamento a nível histórico para eventuais 
consultas, bem como realizara a categorização e sugestões e ao final do fluxo você receberá a notificação contendo o resultado da sua consulta pelo canal escolhido seguindo o padrão acima citado.

Temos como canais : email, webhook, sms ou aplicativos mobile.

<br/>
