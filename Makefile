# Variables
GIT_CURRENT_BRANCH := ${shell git symbolic-ref --short HEAD}
BASE_DIR := ~/Projects/personal/idwall
PYTHONPATH := ${shell pwd}


run:
	@echo "Running insert on Kinesis"
	source $(BASE_DIR)code/utils/stone/bin/activate
	python $(BASE_DIR)code/utils/kinesis.py

setup:
	@echo "Install Requirements"
	source $(BASE_DIR)code/utils/stone/bin/activate
	pip3 install -r $(BASE_DIR)code/utils/requirements.txt

git:
	@echo "Commiting"
	git status
	git add $(BASE_DIR)
	git commit -m "Adding commits"
	git push

terra-elastic-plan:
	@echo "Run Terraform Plan"
	(cd $(BASE_DIR)terraform/elastic_service && terraform plan)

terra-elastic-apply:
	@echo "Run Terraform Apply"
	(cd $(BASE_DIR)terraform/elastic_service && terraform apply -auto-approve)

terra-kinesis-plan:
	@echo "Run Terraform Plan"
	(cd $(BASE_DIR)terraform/kinesis_lambda && terraform plan)


terra-kinesis-apply:
	@echo "Run Terraform Apply"
	(cd $(BASE_DIR)terraform/kinesis_lambda && terraform apply -auto-approve)
