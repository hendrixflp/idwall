import boto3
import json
from datetime import datetime
import time
import pytz
import random
import uuid

BATCH_SIZE = 500
PLATFORM_STREAM = 'kinesis-id-wall'

kinesis_client = boto3.client('kinesis', region_name='us-east-2')

def put_to_stream(partition_key, property_value, json_events, timestamp):
    payload = {
                'prop': str(property_value),
                'timestamp': str(timestamp),
                'event' : json_events
              }


    print('Putting Object to Kinesis ' + PLATFORM_STREAM)

    print("Payload")
    print(str(json.dumps(payload)))

    response = kinesis_client.put_record(
            StreamName=PLATFORM_STREAM,
            Data=json.dumps(payload),
            PartitionKey=partition_key)

    print(str(response))

if __name__ == '__main__':

    count = 0
    #while True:
    while count < BATCH_SIZE:
        now = datetime.now(tz=pytz.timezone('America/Sao_Paulo')).isoformat(),
        property_value = random.randint(40, 120)
        timestamp = now
        partition_key = str(uuid.uuid4())

        json_event = {
            "usuario":{
                "id":"xxx",
            },
            "banco":{
                "tipo_resposta": "endpoint",
                "resposta": "http://xxxx.com",
            },
            "periodo":{
                "data_inicial":"xx/xx/xxxx",
                "data_final":"xx/xx/xxxx",
            },
            "observacoes":"Usuario com alto gastos e alto risco."
        }

        put_to_stream(partition_key, property_value, json_event, timestamp)
        count = count + 1

        time.sleep(1)


    print("Done")